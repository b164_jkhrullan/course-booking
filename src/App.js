import { useState, useEffect } from 'react';
import {Container} from 'react-bootstrap';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Courses from './pages/Courses';
import CourseView from './pages/CourseView';
import ErrorPage from './pages/ErrorPage';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Register from './pages/Register';
import './App.css';
import { UserProvider } from './UserContext';

function App() {

  const [user, setUser] = useState({
      id: null,
      isAdmin: null
  })

  //Function for clearing the localStorage on logout
  const unsetUser = () =>{
    localStorage.clear()
  }

  useEffect(()=> {

      fetch("http://localhost:4000/api/users/details", {
        headers: {
          Authorization:`Bearer ${localStorage.getItem("token")}`
        }
      })
      .then(res => res.json())
      .then(data => {

        if(typeof data._id !== "undefined") {
          setUser({
            id: data._id,
            isAdmin: data.isAdmin
          })
        } else {
          setUser({
            id: null,
            isAdmin: null
          })
        }

      })
  
  }, [])



  return (
    <UserProvider value = {{user, setUser, unsetUser}}>
      <Router>
        <AppNavbar />
        <Container>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/courses" element={<Courses />} />
            <Route path="/courses/:courseId" element={<CourseView/>} />
            <Route path="/login" element={<Login />} />
            <Route path="/register" element={<Register />} />
            <Route path="/logout" element={<Logout />} />
            <Route path="*" element={<ErrorPage />} />
          </Routes>
        </Container>     
      </Router>
    </UserProvider>

  );
}

export default App;
